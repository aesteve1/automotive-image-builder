version: '2'

# We have loaded the variables specified in the manifest, the defaults
# and in the per-arc/target/image-type includes. We can now do optional
# things that depend on what was set in that.

mpp-vars:
  # Backwards compatibility by checking if the deprecated kernel_rpm variable is set
  kernel_core_package:
    mpp-if: locals().get('kernel_rpm')
    then:
      mpp-eval: kernel_rpm + "-core"
    else:
      mpp-eval: kernel_package + "-core"
  kernel_rpm:
    mpp-if: locals().get('kernel_rpm')
    then:
      mpp-eval: kernel_rpm
    else:
      mpp-eval: kernel_package if not kernel_version else kernel_package + "-" + kernel_version
  use_kernel_debug_package:
    mpp-if: "'debug' in locals().get('kernel_package')"
    then: true
  distro_gpg_keys:
    mpp-if: locals().get('distro_gpg_keys')
    then:
      mpp-eval: distro_gpg_keys
    else:
      mpp-format-string: |
        $centos_gpg_key
        $redhat_gpg_key
  # We need to define this ahead of image-ostree.ipp.yml for use below
  use_ostree:
    mpp-eval: use_ostree or image_mode == 'image'
  _dash_escape: "\\x2d"
  boot_mount:
    mpp-format-string: |
      [Unit]
      Before=local-fs.target
      After=blockdev@dev-disk-by\\x2duuid-{bootfs_uuid.replace("-", _dash_escape)}.target

      [Mount]
      What=/dev/disk/by-uuid/$bootfs_uuid
      Where=/boot
      Type=ext4

      [Install]
      RequiredBy=local-fs.target
  boot_efi_mount:
    mpp-format-string: |
      [Unit]
      Before=local-fs.target
      After=blockdev@dev-disk-by\\x2dlabel-ESP.target

      [Mount]
      What=/dev/disk/by-label/ESP
      Where=/boot/efi
      Type=vfat

      [Install]
      RequiredBy=local-fs.target
  var_mount:
    mpp-format-string: |
      [Unit]
      Before=local-fs.target
      After=blockdev@dev-disk-by\\x2duuid-{varpart_uuid.replace("-", _dash_escape)}.target

      [Mount]
      What=/dev/disk/by-uuid/varpart_uuid
      Where=/var
      Type=ext4

      [Install]
      RequiredBy=local-fs.target
  build_rpms:
    mpp-join:
      - mpp-eval: build_rpms
      - - mpp-if: use_luks
          then: cryptsetup
        - mpp-if: use_luks
          then: lvm2
        - mpp-if: use_aboot
          then: aboot-update
        - mpp-if: use_aboot
          then: aboot-deploy
        - mpp-if: use_composefs_signed
          then: openssl
  boot_rpms:
    mpp-join:
      - mpp-eval: boot_rpms
      - mpp-if: use_autoinit
        then:
          - autoinit
      - - $kernel_rpm
  base_rpms:
    mpp-join:
      - mpp-eval: base_rpms
      - - $linux_firmware_rpm
        - mpp-if: use_luks
          then: lvm2
        - mpp-if: use_aboot
          then: aboot-deploy
  dracut_modules:
    - mpp-if: use_autoinit
      then:
        autoinit
  dracut_add_drivers:
    mpp-join:
      - mpp-eval: dracut_add_drivers
      - mpp-if: use_composefs
        then:
          - erofs
          - overlay
          - loop
  dracut_omit_modules:
    mpp-join:
      - mpp-eval: dracut_omit_modules
      - - mpp-if: not use_luks
          then: dm
  kernel_opts:
    mpp-join:
      - mpp-eval: kernel_opts
      - mpp-eval: extra_kernel_opts
      - mpp-if: use_serial_console
        then:
          - console=$serial_console
      - mpp-if: use_debug or use_kernel_debug_package
        then:
          - ignore_loglevel
          - earlycon
      - mpp-if: use_autoinit
        then:
          - autoinit.root=$autoinit_root
          - autoinit.rw
          - mpp-if: use_debug or use_kernel_debug_package
            then: autoinit.debug
      - mpp-if: use_module_sig_enforce
        then:
          # Only allow modules with a valid signature to be loaded by the kernel.
          # https://www.kernel.org/doc/html/latest/admin-guide/module-signing.html#non-valid-signatures-and-unsigned-modules
          - module.sig_enforce=1
      - - systemd.show_status=auto
        # Staggered spin-up
        - libahci.ignore_sss=1
        # Enabling these slub_debug options to address a specific safety concern related to heap memory
        # integrity that only be achieved by setting the SLUB allocator to perform consistency checks.
        #   F = Sanity checks on
        #   P = Poisoning (object and padding)
        #   Z = Add guard areas (red zones) before and after each heap object
        # Note that enabling slub_debug will trigger the following warning on
        # boot up:
        # https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9/-/blob/main/lib/vsprintf.c?ref_type=heads#L2230
        - slub_debug=FPZ
        - fsck.mode=skip
        # Disable rcu_normal_after_boot (because we want to delay the setting of rcu_normal to rcu-normal.service)
        - rcupdate.rcu_normal_after_boot=0
        # Enable rcu_expedited for faster boot (will be disabled by rcu-normal.service)
        - rcupdate.rcu_expedited=1
  image_repos:
    mpp-join:
      - mpp-eval: distro_repos
      - mpp-eval: target_repos
  image_devel_repos:
    mpp-join:
      - mpp-eval: distro_devel_repos
  containers_conf:
    mpp-format-string: |
      [containers]
      { "read_only = true" if containers_read_only else "" }

      default_capabilities = [
        "CHOWN",
        "DAC_OVERRIDE",
        "FOWNER",
        "FSETID",
        "KILL",
        "NET_BIND_SERVICE",
        "SETFCAP",
        "SETGID",
        "SETPCAP",
        "SETUID",
        "SYS_CHROOT"
      ]

      # A list of sysctls to be set in containers by default,
      # specified as "name=value",
      # for example:"net.ipv4.ping_group_range=0 0".
      #
      default_sysctls = [
        "net.ipv4.ping_group_range=0 0",
      ]

      # keyring tells the container engine whether to create
      # a kernel keyring for use within the container.
      #
      keyring = false
  qm_containers_storage_conf:
    mpp-format-string: |
      [storage]
      driver = "overlay"
      runroot = "/run/containers/storage"
      graphroot = "/var/lib/containers/storage"
      { "transient_store = true" if containers_transient_store else "" }

      [storage.options]
      additionalimagestores = [
         { '"' + qm_containers_extra_store + '"' if use_qm_containers_extra_store else "" }
      ]

      [storage.options.overlay]
      mountopt = "nodev,metacopy=on"
  image_enabled_services:
    mpp-join:
      - mpp-eval: image_enabled_services
      - - mpp-if: tmp_is_tmpfs
          then: tmp.mount
        - mpp-if: use_qm or use_bluechi
          then: bluechi-agent.service
        - mpp-if: use_qm or use_bluechi
          then: bluechi-controller.service
        - rcu-normal.service
        - mpp-if: use_efipart
          then: boot-efi.mount
        - mpp-if: use_bootpart
          then: boot.mount
        - mpp-if: use_separate_var
          then: var.mount
  ostree_prepare_root_conf:
    mpp-format-string: |
      [sysroot]
      readonly=true
      [etc]
      { "transient=true" if use_transient_etc else "" }
      [composefs]
      enabled={"maybe" if not use_composefs else "signed" if use_composefs_signed else "yes" }
  build_info:
    mpp-format-string: |
      RELEASE="{release_name}"
      UUID="{image_uuid}"
      TIMESTAMP="{build_timestamp}"
      IMAGE_NAME="{name}"
      IMAGE_MODE="{image_mode}"
      IMAGE_TARGET="{target}"
  qm_importfile:
    mpp-if: use_qm
    then: "qm"
    else: "empty"
  use_bluechi:
    mpp-if: use_qm
    then: true
    else:
      mpp-eval: use_bluechi
  bluechi_agent_conf:
    mpp-format-string: |
      [bluechi-agent]
      NodeName={ bluechi_nodename }
      ManagerHost= { bluechi_manager_host_ip }
  qm_bluechi_agent_conf:
    mpp-format-string: |
      [bluechi-agent]
      NodeName=qm.{ bluechi_nodename }
      ManagerHost= { bluechi_manager_host_ip }
  bluechi_conf:
    mpp-format-string: |
      [bluechi-controller]
      AllowedNodeNames={ bluechi_nodename },qm.{ bluechi_nodename }
  qm_subuid_content:
    mpp-format-string: |
      containers:{ qm_container_subuid }
  qm_subgid_content:
    mpp-format-string: |
      containers:{ qm_container_subgid }
  subuid_content_qm:
    mpp-if: use_qm
    then:
      mpp-format-string: |-
        qmcontainers:{qm_container_subuid}
        containers:{container_subuid}
    else: ""
  subgid_content_qm:
    mpp-if: use_qm
    then:
      mpp-format-string: |-
        qmcontainers:{qm_container_subgid}
        containers:{container_subgid}
    else: ""
  subuid_content: |-
    $subuid_content_qm
    $extra_subuid
  subgid_content: |-
    $subgid_content_qm
    $extra_subgid
  systemd_system_conf:
    mpp-format-string: |
      [Manager]
      DefaultTimeoutStartSec={systemd_timeout}
      DefaultTimeoutStopSec={systemd_timeout}
      DefaultTimeoutAbortSec={systemd_timeout}
      DefaultRestartSec={systemd_timeout}
      DefaultDeviceTimeoutSec={systemd_timeout}
  coredump_conf:
    mpp-format-string: |
      [Coredump]
      Storage={coredump_storage}
  rootpart_label:
    mpp-eval: aboot_partlabel if use_aboot else "root"
  main_nmconnection:
    mpp-format-string: |
      [connection]
      type=ethernet
      id=Main connection
      uuid={static_ip_uuid}
      autoconnect=true
      { "interface-name = " + static_ip_iface if static_ip_iface else "" }

      [ipv4]
      method=manual
      address={static_ip}
      dns={static_dns}
      gateway={static_gw}

      [ipv6]
      method=disabled

pipelines: []
